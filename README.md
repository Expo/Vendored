[![](https://codeberg.org/Expo/vendored/raw/branch/senpai/social.png)](#expove)

# @expove

Packages I vendor, be it for security/supply chain, or due to features in master I want early.
